const express = require('express')
const path = require('path')

const app = express();
const port = process.env.PORT || 3000;

// SENDFILE will go here

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname,'index.html'));
})
